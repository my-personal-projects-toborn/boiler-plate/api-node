# Docker Container Name

Descrição do Container.

## Getting Started

Instruções do Container.

### Prerequisities

Para executar este container deverá ter o Docker instalado.

- [Windows](https://docs.docker.com/windows/started)
- [OS X](https://docs.docker.com/mac/started/)
- [Linux](https://docs.docker.com/linux/started/)

O docker-compose deverá ser instalado separadamente.

- [Docker](https://docs.docker.com/compose/install/)

Para utilização do ESLint deverá seguir as instruções.

- [ESLint](https://eslint.org/docs/user-guide/getting-started/)

Para utilização do Prettier deverá seguir as instruções.

- [Prettier](https://prettier.io/docs/en/install.html)

Para utilização do Nodemon deverá seguir as instruções.

- [Nodemon](https://www.npmjs.com/package/nodemon)

Para utilização do Yarn deverá seguir as instruções.

- [Yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable)

### Usage

#### Container Parameters

Lista dos parametros do sistema.

##### Iniciar Aplicação

```shell
docker-compose up
```

#### Environment Variables

- `NODE_ENV` - Ambiente de desenvolvimento.
- `PORT` - Porta da aplicação.
- `DB_NAME` - Nome do Banco de Dados.
- `DB_USER` - Usuario de Acesso do Banco de Dados.
- `DB_PASSWORD` - Senha de Acesso do Banco de Dados.
- `DB_HOST` - Host do Banco de Dados.
- `DB_DIALECT` - Banco de Dados utilizado.
- `JWT` - Chave para criptografia.
- `TOKENTIME` - Tempo de Expiração do token de Acesso.
- `STORAGE_TYPE` - Tipo de armazenamendo de arquivo.
- `MAX_FILE_SIZE` - Tamanho máximo de arquivo aceito para armazenamento.
- `BUCKET_NAME` - Nome do Bucket caso armazenamento seja na nuvem.
- `AWS_ACCESS_KEY_ID` - Chave de acesso ao Bucket.
- `AWS_SECRET_ACCESS_KEY` - Chave de acesso ao serviço da AWS.
- `AWS_DEFAULT_REGION` - Região em que o Bucket está localizado.

#### Volumes

- `/usr/app` - Local do Volume

#### Useful File Locations

- `/logs/` - Logs da Aplicação
- `/tmp/` - Armazenamento de arquivo local
- `/src/api/controllers` - Funções da controllers do express route
- `/src/api/models` - Modelos do banco de dados
- `/src/api/services` - Regras de negócio
- `/src/api/subscribers` - Eventos async
- `/src/api/repositories` - Query builders
- `/config` - Configuração das variaveis de ambiente
- `/jobs` - Tarefas de rotinas
- `/loaders` - Modulos para utilizado no app
- `/utils` - Trechos de código pequeno
- `/helpers` - Trechos de arquitetura de código
- `/routes` - Definição de rotas express

## Built With

- node v15.12.0
- docker v20.10.1
- docker-compose v1.25.5

## Find Us

- [GitLab](https://gitlab.com/Toborn/)

## Versioning

Utilizamos [SemVer](http://semver.org/) para versionamento.

## Authors

- **Marcio Braga** - _Initial work_ - [Toborneiro](https://gitlab.com/Toborn)

## Acknowledgments

- Padrões baseados de [Github](https://github.com/armoucar/javascript-style-guide)
