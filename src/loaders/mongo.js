import mongoose from 'mongoose';
import logger from '../config/logger';

export default mongoose.connect(process.env.DB_NAME, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

mongoose.connection.on('error', (err) => {
  throw new Error(err);
});

mongoose.connection.once('open', () => {
  logger.info('DB Connected');
});
