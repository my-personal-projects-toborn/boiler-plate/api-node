import morgan from 'morgan';
import logger from './logger';

const stream = {
  write: (message) => logger.http(message),
};

const skip = () => {
  const env = process.env.NODE_ENV || 'development';
  return env !== 'development';
};

const jsonFormat = (tokens, req, res) =>
  JSON.stringify({
    time: tokens.date(req, res, 'iso'),
    method: tokens.method(req, res),
    url: tokens.url(req, res),
    'status-code': tokens.status(req, res),
    'content-length': tokens.res(req, res, 'content-length'),
    referrer: tokens.referrer(req, res),
    'response-time': tokens['response-time'](req, res),
  });

export default morgan(jsonFormat, { stream, skip });
