/**
Importo a biblioteca express;
Importo as rotas externas e seus demais verbos (POST, GET, PUT, DELETE);
* */
import express from 'express';
import cors from 'cors';
import routes from './routes';
import morgan from './config/morgan';

function start() {
  const server = express();
  server.use(express.json());
  server.use(cors());
  server.use(morgan);
  server.use(routes);
  return server;
}

export default start();
