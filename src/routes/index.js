import { Router } from 'express';

const router = new Router();

router.get('/status', (req, res) =>
  res.status(200).json({ message: 'Online' })
);

router.use((req, res) => {
  res.status(404).send({
    error: true,
    message: 'Not Found',
    detail: 'URI Not Found. Try /status',
  });
});

export default router;
