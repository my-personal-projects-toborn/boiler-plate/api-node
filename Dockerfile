FROM node:16 AS base

WORKDIR /usr/src/app

FROM base AS builder

COPY package*.json ./
COPY yarn.lock ./
RUN yarn install
COPY ./src ./src
RUN yarn build
RUN npm prune --production

FROM base AS release

COPY --from=builder /usr/src/app/node_modules ./node_modules
COPY --from=builder /usr/src/app/build ./build
RUN ls -a

USER node

CMD ["yarn", "serve"]